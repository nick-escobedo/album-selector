export const discography = {

  "album": [
    {
      title:"Roman Candle",
      albumCoverSrc:"romancandle-cover.jpg",
      albumVinylSrc:"romancandle-vinyl.png",
      releaseDate:"1994",
      fact1:"Roman Candle was recorded and released while Smith was still in Heatmiser. According to Benjamin Nugent's biography Elliott Smith and the Big Nothing, Smith recorded the album in the basement of the home of then-girlfriend and Heatmiser manager J.J. Gonson.",
      fact2:'The album was never intended for release, as Smith only expected to get a deal for a 7" single; however, after Gonson played the album for Cavity Search, they immediately requested permission to release it in its entirety. Smith at first hesitated, and then allowed permission.[2]',
      fact3:'The album has a raw, homemade sound (see lo-fi music), with Smith playing each instrument and recording it on his four-track recorder.[3] Additionally he used an inexpensive Radioshack dynamic microphone to capture the sound.',
      fact4:'The front cover features a photograph taken of Neil Gust (of Heatmiser) and friend Amy Dalsimer by Gonson. Smith chose the image because he "liked the way the picture looked as a piece of art".[2]',
      fact5:'Roman Candle was released on July 14, 1994.'
    },
    {
      title:"Elliot Smith",
      albumCoverSrc:"esmith-cover.jpg",
      albumVinylSrc:"esmith-vinyl.png",
      releaseDate:"1995",
      fact1:'Smith mostly appears alone on his acoustic guitar, although he is occasionally backed up by the odd musical instrument, such as a harmonica and drums.',
      fact2:'The album\'s lyrics contain many references to drug use, which Smith claims are metaphorical. The album cover features a photograph by J. J. Gonson, who also photographed the cover for Roman Candle, depicting cut-out figures falling from buildings as if they were committing suicide.[1]',
      fact3:'Elliott Smith was released on July 21 through Kill Rock Stars, making it his first full-length album on the label.[6] In contrast to Roman Candle, Elliott Smith was "promoted heavily", with posters of Smith appearing in the windows of record stores across the Northwest of Portland, Oregon, where Smith lived at the time.[1]',
      fact4:'"Needle in the Hay" appeared in Wes Anderson\'s 2001 film The Royal Tenenbaums, in a scene featuring a suicide attempt. Smith was reportedly unhappy about the song being used this way.[5] The song appeared on the film\'s soundtrack.[6]',
      fact5:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, maxime consequuntur officiis. Consectetur porro, fugit tempora qui? Dolore repellat esse repudiandae perferendis voluptas quia maxime! Vitae facere, in sit quam?'
    },
    {
      title:"Either/Or",
      albumCoverSrc:"eitheror-cover.jpg",
      albumVinylSrc:"eitheror-vinyl.png",
      releaseDate:"1997",
      fact1:'Either/Or is the third studio album by American singer-songwriter Elliott Smith.',
      fact2:'Recorded in several locations mostly in Portland, Oregon while Smith was still in Heatmiser and produced by Smith, Tom Rothrock and Rob Schnapf, Either/Or was released on February 25, 1997 through record label Kill Rock Stars following the demise of Heatmiser. ',
      fact3:'The album\'s title derives from the Søren Kierkegaard book of the same name, reflecting Smith\'s interest in philosophy, which he studied at Hampshire College in Massachusetts.[3]',
      fact4:'Either/Or was released on February 25, 1997.',
      fact5:'Smith would be cast into the international spotlight early the following year when he performed his song, the 1997 standalone single "Miss Misery", at the 1998 Academy Awards.'
    },
    {
      title:"XO",
      albumCoverSrc:"xo-cover.jpg",
      albumVinylSrc:"xo-vinyl.png",
      releaseDate:"1998",
      fact1:'An early working title for the album was Grand Mal.',
      fact2:'The title of the first track, "Sweet Adeline", was inspired by Smith’s recollections of his grandmother singing in her glee club, Sweet Adelines International.',
      fact3:'XO was released by DreamWorks Records on August 25, 1998',
      fact4:'BBC Music wrote, "the budget might have gone up, but Smith\'s masterful way with an understated melody and melancholic lyric remained firmly intact", calling XO "perhaps the greatest long-player Smith released; if not, it\'s certainly the equal of the preceding Either/Or. ',
      fact5:'"Amity" is believed to be named after a friend who can be seen in photographs from Smith\'s 1997 tour.[6]'
    },
    {
      title:"Figure 8",
      albumCoverSrc:"fig8-cover.jpg",
      albumVinylSrc:"fig8-vinyl.png",
      releaseDate:"2000",
      fact1:'Figure 8 was Elliott Smith’s misunderstood brush with mainstream success and the final record he completed before his tragic death in 2003.',
      fact2:'The Nebraska-born, Texas-bred, Portland-based singer had won a cult-like following with albums of close-whispered suffering.',
      fact3:'The period in which he recorded the album was reportedly one of the most troubled in his life.',
      fact4:'Rolling Stone gave Figure 8 just three and a half stars on release but named it the 42nd best album of the 2000s, calling it a “haunted high-water mark.” Pitchfork, who rated it 6.9 on release, later called it, “one of Smith’s most accessible and enjoyable records.”',
      fact5:'Elliott Smith’s Figure 8 was released on April 18, 2000 and peaked at #45 on the ARIA charts.'
    },
    {
      title:"From A Basement On A Hill",
      albumCoverSrc:"fboh-cover.jpg",
      albumVinylSrc:"fboh-vinyl.png",
      releaseDate:"2004",
      fact1:'From a Basement on the Hill is the sixth and final studio album by the late American singer-songwriter Elliott Smith.',
      fact2:'Recorded between 2002 and 2003, it was released posthumously in the UK and Europe on Domino on October 18',
      fact3:'The album was initially planned as a double album, due to contractual obligations with the DreamWorks label (now Interscope), and was incomplete at the time of Smith\'s death.',
      fact4:'During the recording period for the album, Smith had recorded with Steven Drozd and Russell Simins, drummers for The Flaming Lips and Jon Spencer Blues Explosion, respectively.',
      fact5:'After his death and the release of From a Basement on the Hill, many critics and fans viewed the album as a suicide note.'
    },
    {
      title:"New Moon",
      albumCoverSrc:"newmoon-cover.jpg",
      albumVinylSrc:"newmoon-vinyl.png",
      releaseDate:"2007",
      fact1:'New Moon is a posthumous compilation album by American singer-songwriter Elliott Smith, released on May 8, 2007 by Kill Rock Stars.',
      fact2:' It contains twenty-four previously unreleased songs, most recorded between 1994 and 1997 during the sessions for Smith\'s albums Elliott Smith and Either/Or.',
      fact3:'Smith\'s family hired his former producer Rob Schnapf and ex-girlfriend Joanna Bolme to sort through and put the finishing touches on the batch of over thirty songs that were recorded for the album, although the estate retained final decision of which tracks to include.[3] Thus, a fifteen-track album was assembled and released.',
      fact4:'His death was presumed to be a suicide, although the coroner\'s report and statements from close friends reveal that his death is still under speculation. ',
      fact5:'The album was well received by critics and reached number 24 in the US Billboard chart, selling about 24,000 copies in its first week.'
    },
  ]

}

