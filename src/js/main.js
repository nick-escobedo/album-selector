import { discography } from './esmith-discography';

let initial = true;

function loadImages(data) {
  const album = data.album;
  for (let i = 0; i < album.length; i += 1) {
    // Slide Div
    const albumSlideDiv = document.createElement('div');
    albumSlideDiv.setAttribute('class', 'albumSlide');
    albumSlideDiv.setAttribute('data-slideid', `${ i + 1}`);

    // Titlebar
    const titleBar = document.createElement('div');
    titleBar.setAttribute('class','titlebar');

    // Release Date
    const releaseDate = document.createElement('div');
    releaseDate.setAttribute('class', 'release-date');
    releaseDate.innerHTML = album[i].releaseDate;

    // Title
    const title = document.createElement('div');
    title.setAttribute('class', 'album-title');
    title.innerHTML = album[i].title;

    // Album Cover
    const coverImg = document.createElement('img');
    coverImg.setAttribute('class', 'album-cover');
    coverImg.src = `img/${album[i].albumCoverSrc}`;

    // Record Iimage
    const vinylImg = document.createElement('img');
    vinylImg.setAttribute('class', 'album-vinyl');
    vinylImg.src = `img/${album[i].albumVinylSrc}`;

    // Facts Ul/LI
    const ul = document.createElement('ul');
    ul.setAttribute('class', 'ul-album');

    const fact1 = document.createElement('li');
    fact1.setAttribute('class', 'fact');
    fact1.innerHTML = album[i].fact1;

    const fact2 = document.createElement('li');
    fact2.setAttribute('class', 'fact');
    fact2.innerHTML = album[i].fact2;

    const fact3 = document.createElement('li');
    fact3.setAttribute('class', 'fact');
    fact3.innerHTML = album[i].fact3;

    const fact4 = document.createElement('li');
    fact4.setAttribute('class', 'fact');
    fact4.innerHTML = album[i].fact4;

    // Append to albumSlide div
    document.getElementById('album-facts').appendChild(albumSlideDiv);
    albumSlideDiv.appendChild(titleBar);
    titleBar.appendChild(releaseDate);
    titleBar.appendChild(title);
    albumSlideDiv.appendChild(ul);
    ul.appendChild(coverImg);
    ul.appendChild(vinylImg);
    ul.appendChild(fact1);
    ul.appendChild(fact2);
    ul.appendChild(fact3);
    ul.appendChild(fact4);

    // Add dots
    let dots = document.createElement('img');
    dots.src = `img/${album[i].albumCoverSrc}`;

    coverImg.setAttribute('class', 'album-cover');
    dots.setAttribute('class', 'dot');
    dots.setAttribute('data-albumid', `${i + 1}`);
    // dots.setAttribute('onclick', 'currentSlide(' + (i + 1) + ')');
    // dots.setAttribute('onclick', function () { currentSlide() });
    dots.addEventListener('click', (e) => getCurrentThumb(e));
    document.getElementById('dots').appendChild(dots);
  }
  if (initial) {
    showSlides(1)  // grab first slide
    initial = false;
  }

}

loadImages(discography);


// PREVIOUS - NEXT BUTTONS
const getCurrentSlide = () => (
   parseInt(document.querySelector('.albumSlide.active').getAttribute('data-slideid'), 10));
const getSlide = (direction) => showSlides(getCurrentSlide() + direction);

const next = document.getElementById('next');
next.addEventListener('click', () => getSlide(1));

const prev = document.getElementById('prev');
prev.addEventListener('click', () => getSlide(-1));



let getCurrentThumb = (e) => showSlides(parseInt(e.target.getAttribute('data-albumid'), 10));



function showSlides(n) {
  let slideIndex = n;

  let i;

  const slides = [...document.querySelectorAll('.albumSlide')];

  // LOOK UP DESTRUCURING
  // const dots = document.getElementsByClassName('dot');  look up .apply / .call
    const dots = [...document.getElementsByClassName('dot')];

  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }

  slides.map(slide => {
    slide.classList.remove('active');
    slide.style.display = 'none';
  });
  // slides.map(slide => myfunc(slide))

  // for (i = 0; i < slides.length; i += 1) {
  //   slides[i].classList.remove('active');
  //   slides[i].style.display = 'none';
  // }


  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }



  let targetSlide = document.querySelector(`div[data-slideid='${slideIndex}'`);
  console.log('yo, target slide is: ', targetSlide);
  targetSlide.style.display = 'block';

  targetSlide.className += ' active';
  slides.filter(slide => !slide.classList.contains('active'))
    .map((slide) => {
    slide.classList.remove('active');
  });
  dots[slideIndex - 1].className += ' active';


}


function changeSlides() {
  dots.addEventListener('click', (e) => {
    var slideID = e.currentTarget;

    console.log('you clicky the thumbnail!');
    console.log(slideID);
  });
}




// Ul list switch layout style
const switchUl = document.getElementsByClassName('ul-album');
const switchCover = document.getElementsByClassName('album-cover');
const switchVinyl = document.getElementsByClassName('album-vinyl');

document.addEventListener('DOMContentLoaded', function(e) {
  setTimeout(function() {
    return switchUl.className += 'w_image';
  }, 500);

});



// function navFixed() {
//   if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
//     document.getElementById("navbares").className = 'fixed';
//   } else {
//     document.getElementById("navbares").className = '';
//   }
// }

// function offsetter() {
//   const balloon = document.getElementById('balloon');

//   if (window.scrollY > 350) {
//     Balloon.style.display = 'inline-block';
//     // baloon.className += " popped";
//     console.log("popped this shit");
//     this.removeEventListener()
//   } else {
//     // Balloon.style.display = 'none';
//   }

//   // baloon.className += " popped";

// }

// offsetter();


// document.addEventListener("scroll", function(e) {

//   const balloon = document.getElementById('balloon');

//   function offsetter3() {
//     if (window.scrollY > 350) {
//       // Balloon.style.display = 'inline-block';
//       baloon.className += " popped";
//       console.log("popped this shit");
//       this.removeEventListener()
//     } else {
//       // Balloon.style.display = 'none';
//     }

//  }

//   offsetter3();

// });


// const showBaloon = () => {
//   if (Balloon) {
//     if (window.pageYOffset > 250) {
//       Balloon.style.display = 'inline-block';
//     } else {
//       Balloon.style.display = 'none';
//     }
//   }
// };

// showBaloon();



const lboxImage = document.querySelector('.lightbox-link');

const overlay = document.querySelector('.overlay');
const close = document.querySelector('#close');
const light = document.querySelector('#light')

lboxImage.addEventListener('click', function() {
  console.log('lightbox clicked');
  console.log(overlay);
  overlay.setAttribute('style', 'display:block');
  light.setAttribute('style', 'display:block');
  // overlay.className += ' active';



});


overlay.addEventListener('click', function() {
  console.log("overlay clicked");
  overlay.setAttribute('style', 'display:none');
  light.setAttribute('style', 'display:none');
});

close.addEventListener('click', function() {
  overlay.setAttribute('style', 'display:none');
  light.setAttribute('style', 'display:none');
})


// BALLOON FLYAWAY


const balloon = document.getElementById('balloon');
var topPos = balloon.offsetTop;

balloon.addEventListener('click', function() {
  if (topPos > 10) {
    console.log('balloon clicked');
    // object is offset more
    // than 10 pixels from its parent
  }
});





// COMING UP ROSES
const elm = document.getElementById('balloon');

function posY(elm) {
    var test = elm, top = 0;

    while(!!test && test.tagName.toLowerCase() !== "body") {
        top += test.offsetTop;
        test = test.offsetParent;
    }

    return top;
}

function viewPortHeight() {
    var de = document.documentElement;

    if(!!window.innerWidth)
    { return window.innerHeight; }
    else if( de && !isNaN(de.clientHeight) )
    { return de.clientHeight; }

    return 0;
}

function scrollY() {
    if( window.pageYOffset ) { return window.pageYOffset; }
    return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
}

function checkvisible( elm ) {
    var vpH = viewPortHeight(), // Viewport Height
        st = scrollY(), // Scroll Top
        y = posY(elm);

    return (y > (vpH + st));
}