
const gulp = require('gulp');
const babel = require('gulp-babel');
const rollup = require('gulp-rollup');
const sass = require('gulp-sass');
const notify = require('gulp-notify');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const clean = require('gulp-clean');

// Static Server + watching scss/html files
gulp.task('serve', function() {
  browserSync.init({
    server: './public',
    open: true,
    port: 8080,
  });

  gulp.watch('src/scss/*.scss', ['sass']);
  gulp.watch('src/js/*.js', ['minify-js']).on('change', browserSync.reload);
  gulp.watch("public/*.html").on('change', browserSync.reload);
});

// Compile Our Sass
gulp.task('sass', function() {
  return gulp.src('src/scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/css'))
    .pipe(notify({ message: 'SASS BE CSSSED!!' }))
    .pipe(reload({ stream: true }));
});


// Minify task
gulp.task('minify-js', function () {
  return gulp.src('src/js/*.js') // path to your files
    .pipe(rollup({ entry: 'src/js/main.js' }))
    .pipe(babel({
      presets: ['es2015'],
    }))
    // .pipe(uglify())
    .pipe(gulp.dest('public/js'));

  console.log('sheeeet');
});


gulp.task('hello', function() {
  console.log('Hello Zell');
});

// Default Task
// gulp.task('default', ['serve','sass', 'watch']);
gulp.task('default', ['serve', 'clean','minify-js','sass', 'watch']);

// Watch Files For Changes
gulp.task('watch', function() {
  // gulp.watch('public/*.css', ['sass']);
});

gulp.task('clean', function() {
  return gulp.src('public/js/*')
      .pipe(clean());
});
